﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SistemaArtigo.Models
{
    public class CartaoCredito
    {
        public int Numero { get; set; }
        public DateTime Validade { get; set; }
        public string Bandeira { get; set; }
    }
}