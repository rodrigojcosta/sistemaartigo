﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SistemaArtigo.Models
{
    public class Pessoa
    {
        [Required]
        public string Nome { get; set; }

        [Required]
        public int Telefone { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        [Display(Name ="Local De Participação")]
        public string LocalParticipacao { get; set; }

        [Required]
        
        public Endereco Endereco { get; set; }

        [Required]
        public CartaoCredito CartaoCredito { get; set; }

       [Required]
        public string Senha { get; set; }
        [Required]
        [Display(Name = "Repita a sua senha:")]
        public string RepitaSenha { get; set; }
    }
}